<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-06-15T15:14:28</lastRun>
   <mailRecipient>venkatesh.yalavatthi@widas.in;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>29ba6258-bfa7-4d32-b89c-3a6072c88c75</testSuiteGuid>
   <testCaseLink>
      <guid>ec25571c-038a-4fdd-b555-f793f10860f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/User_UI/Login_Logout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e3c9274-5cee-4615-9d26-39b76ea14b49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/User_UI/Change_Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e9daf45-564c-42cc-9acd-73ca647551f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/User_UI/LoginwithInvalidpassword</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa378eda-043e-4168-a591-705299c3afd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/User_UI/ProfileUpdation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d12ca184-6d9f-48c4-96bd-469854150346</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User_UI/Forgot_Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>466791c5-1629-491c-9670-b9c37551d461</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/User_UI/Registration</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
