<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_form-control ng-untouched ng-pristine ng-valid</name>
   <tag></tag>
   <elementGuidId>e4ee8ccd-373e-4520-8c32-7b661640d582</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-control ng-untouched ng-pristine ng-valid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-untouched ng-pristine ng-valid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>20</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;parent_div&quot;)/form[@class=&quot;ng-invalid ng-dirty ng-touched&quot;]/div[2]/div[7]/div[@class=&quot;form-group&quot;]/int-phone-prefix[@class=&quot;ng-valid ng-touched ng-dirty&quot;]/div[@class=&quot;input-group&quot;]/input[@class=&quot;form-control ng-untouched ng-valid ng-dirty&quot;]</value>
   </webElementProperties>
</WebElementEntity>
