<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span__fc_3 owaimg ms-Icon--sea</name>
   <tag></tag>
   <elementGuidId>23735504-75b6-4dc8-b96b-6d734fe43984</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>_fc_3 owaimg ms-Icon--search ms-icon-font-size-20 ms-fcl-ts-b</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;primaryContainer&quot;)/div[4]/div[@class=&quot;conductorContent&quot;]/div[@class=&quot;_n_T&quot;]/div[@class=&quot;_n_X&quot;]/div[1]/div[1]/div[1]/div[@class=&quot;_n_R2 ms-bgc-nlr _n_T2&quot;]/div[@class=&quot;_n_g3&quot;]/div[1]/div[@class=&quot;_n_S2&quot;]/div[@class=&quot;_n_13 _n_63&quot;]/div[@class=&quot;_n_83&quot;]/div[@class=&quot;_n_o&quot;]/div[@class=&quot;_is_o ms-bcl-tp _is_q&quot;]/div[@class=&quot;_is_t ms-bgc-w ms-bcl-tp&quot;]/button[@class=&quot;_is_w o365button&quot;]/span[@class=&quot;_fc_3 owaimg ms-Icon--search ms-icon-font-size-20 ms-fcl-ts-b&quot;]</value>
   </webElementProperties>
</WebElementEntity>
