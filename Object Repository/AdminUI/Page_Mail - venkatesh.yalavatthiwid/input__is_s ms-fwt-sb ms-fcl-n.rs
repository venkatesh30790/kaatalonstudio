<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__is_s ms-fwt-sb ms-fcl-n</name>
   <tag></tag>
   <elementGuidId>678dfcd6-ef08-4e6a-98e5-3ff6c9cf11e1</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autoid</name>
      <type>Main</type>
      <value>_is_3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>_is_s ms-fwt-sb ms-fcl-np ms-bgc-w ms-bgc-w-h allowTextSelection textbox ms-font-s ms-fwt-sl ms-bcl-nta ms-bcl-nsa-h hideClearButton</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>combobox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Search mail and people, type your search term then press enter to search.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;primaryContainer&quot;)/div[4]/div[@class=&quot;conductorContent&quot;]/div[@class=&quot;_n_T&quot;]/div[@class=&quot;_n_X&quot;]/div[1]/div[1]/div[1]/div[@class=&quot;_n_R2 ms-bgc-nlr _n_T2&quot;]/div[@class=&quot;_n_g3&quot;]/div[1]/div[@class=&quot;_n_S2&quot;]/div[@class=&quot;_n_13 _n_63&quot;]/div[@class=&quot;_n_83&quot;]/div[@class=&quot;_n_o&quot;]/div[@class=&quot;_is_o ms-bcl-tp _is_q&quot;]/div[@class=&quot;_is_t ms-bgc-w ms-bcl-tp&quot;]/form[1]/div[@class=&quot;_is_r&quot;]/input[@class=&quot;_is_s ms-fwt-sb ms-fcl-np ms-bgc-w ms-bgc-w-h allowTextSelection textbox ms-font-s ms-fwt-sl ms-bcl-nta ms-bcl-nsa-h hideClearButton&quot;]</value>
   </webElementProperties>
</WebElementEntity>
