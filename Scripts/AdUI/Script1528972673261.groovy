import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://nightlybuild.cidaas.de/admin-ui/login?view_type=login&requestId=4a0dfae7-da95-4a40-8123-20837dc2978b')

WebUI.setText(findTestObject('AdminUI/Page_Login/input_username'), 'venkatesh.yalavatthi@widas.in')

WebUI.setText(findTestObject('AdminUI/Page_Login/input_password'), '123456')

WebUI.click(findTestObject('AdminUI/Page_Login/button_Login'))

WebUI.click(findTestObject('AdminUI/Page_Cidaas Developers/a_Email'))

WebUI.navigateToUrl("https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&response_mode=form_post&response_type=code+id_token&scope=openid+profile&state=OpenIdConnect.AuthenticationProperties%3dmcg3ngLkBAPjiQ8XQSR7MuwTlU0Bn8ribfwh4skhEVcyZiDdgDv7E9b85QbYyZM1u_hYI9c8_A4FcWAmXEfZhxc6cyb9rWJHcJsAGpRlRtANf5iQDx8gqw8u4d28Tno4&nonce=636645693386591493.NTlhM2I2Y2QtOTkxYS00ZTQ0LWFjOGMtY2I3NWVmZmI5MmRmZDNiMTBhNTMtNmI5NS00MWIwLWFmYTYtMTlkY2ZhMWVhZjY0&redirect_uri=https%3a%2f%2fwww.office.com%2f&ui_locales=en-US&mkt=en-US&client-request-id=c1031765-e2d9-4dfc-9101-d807a4c0e956")

WebUI.maximizeWindow()

WebUI.delay(5)



WebUI.setText(findTestObject('AdminUI/Page_Sign in to your account/input_loginfmt'), 'venkatesh.yalavatthi@widas.in')

WebUI.click(findTestObject('AdminUI/Page_Sign in to your account/input_idSIButton9'))

WebUI.setText(findTestObject('AdminUI/Page_Sign in to your account/input_passwd'), 'Microsoft123')

WebUI.click(findTestObject('AdminUI/Page_Sign in to your account/input_idSIButton9'))

WebUI.click(findTestObject('AdminUI/Page_Sign in to your account/input_idSIButton9'))

WebUI.click(findTestObject('AdminUI/Page_Microsoft Office Home/span_ms-ohp-Icon ms-ohp-Icon--'))

WebUI.switchToWindowTitle('Mail - venkatesh.yalavatthi@widas.in')

WebUI.delay(6)

WebUI.setText(findTestObject('AdminUI/Page_Microsoft Office Home/input__is_s ms-fwt-sb ms-fcl-np ms-bgc-w ms-bgc-w-h allowTextSelection textbox ms-font-s ms-fwt-sl ms-bcl-nta ms-bcl-nsa-h hideClearButton'), 'noreply@cidaas.de')

WebUI.delay(6)

WebUI.click(findTestObject('AdminUI/Page_Microsoft Office Home/span__fc_3 owaimg ms-Icon--search ms-icon-font-size-20 ms-fcl-ts-b'))





//WebUI.click(findTestObject('AdminUI/Page_Mail - venkatesh.yalavatthiwid/span__fc_3 owaimg ms-Icon--sea'))
//
//WebUI.click(findTestObject('AdminUI/Page_Mail - venkatesh.yalavatthiwid/div_Draft     noreplycidaas.de'))
//
//WebUI.click(findTestObject('AdminUI/Page_Mail - venkatesh.yalavatthiwid/div_Hi Venkatesh Y1 Your MFA A'))
//
//WebUI.rightClick(findTestObject('AdminUI/Page_Mail - venkatesh.yalavatthiwid/div_Hi Venkatesh Y1 Your MFA A'))
//
//WebUI.click(findTestObject('AdminUI/Page_Mail - venkatesh.yalavatthiwid/div_Welcome to Cidaas Develope'))
//
//WebUI.click(findTestObject('AdminUI/Page_Mail - venkatesh.yalavatthiwid/div_Hi Venkatesh Y1 Your MFA A'))

WebUI.closeBrowser()

