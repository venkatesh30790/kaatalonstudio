import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://nightlybuild.cidaas.de/user-ui/login?groupname=default&lang=en-US,en;q%3D0.9&view_type=login&redirect_uri=https:%2F%2Fnightlybuild.cidaas.de%2Fuser-profile%2Feditprofile&client_id=4d5e6e20-9347-4255-9790-5b7196843103&response_type=token&scope=openid%20profile&requestId=aced207a-9c3a-4ac8-9aa5-8df6a2442b5a')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('ProfileUpdate/Page_Cidaas Developers/input_username'), 'vyalavatti@gmail.com')

WebUI.setText(findTestObject('ProfileUpdate/Page_Cidaas Developers/input_password'), '12345678')

WebUI.click(findTestObject('ProfileUpdate/Page_Cidaas Developers/button_Login'))

WebUI.doubleClick(findTestObject('ProfileUpdate/Page_User Profile/input_ng-untouched ng-pristine'))

WebUI.setText(findTestObject('ProfileUpdate/Page_User Profile/input_ng-untouched ng-pristine'), 'vyalavatthi')

WebUI.doubleClick(findTestObject('ProfileUpdate/Page_User Profile/input_ng-untouched ng-pristine_1'))

WebUI.setText(findTestObject('ProfileUpdate/Page_User Profile/input_ng-untouched ng-pristine_1'), 'srivatsav')

WebUI.doubleClick(findTestObject('ProfileUpdate/Page_User Profile/input_ng-untouched ng-pristine_2'))

WebUI.setText(findTestObject('ProfileUpdate/Page_User Profile/input_ng-untouched ng-pristine_2'), 'Test2')

WebUI.click(findTestObject('ProfileUpdate/Page_User Profile/button_Save'))

WebUI.click(findTestObject('ProfileUpdate/Page_User Profile/button_OK'))

WebUI.delay(6)

WebUI.verifyTextPresent("vyalavatti@gmail.com", true)

WebUI.click(findTestObject('Login_Logou/Page_User Profile/a_Logout'))

WebUI.click(findTestObject('Login_Logou/Page_User Profile/button_YES'))


WebUI.closeBrowser()

