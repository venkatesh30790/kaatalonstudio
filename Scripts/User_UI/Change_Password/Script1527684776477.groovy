import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.codehaus.groovy.ast.stmt.CaseStatement as CaseStatement
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://nightlybuild.cidaas.de/user-ui/login?groupname=default&lang=en-US,en;q%3D0.9&view_type=login&redirect_uri=https:%2F%2Fnightlybuild.cidaas.de%2Fuser-profile%2Feditprofile&client_id=4d5e6e20-9347-4255-9790-5b7196843103&response_type=token&scope=openid%20profile&requestId=aced207a-9c3a-4ac8-9aa5-8df6a2442b5a')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('change_Password/Page_Cidaas Developers/input_username'), 'vyalavatti@gmail.com')

WebUI.setText(findTestObject('change_Password/Page_Cidaas Developers/input_password'), '123456')

WebUI.click(findTestObject('change_Password/Page_Cidaas Developers/button_Login'))

try {
    //WebUI.click(findTestObject('change_Password/Page_User Profile/p_Invalid username or password'))
    WebUI.verifyElementPresent(findTestObject('change_Password/Page_User Profile/p_Invalid username or password'), 10)
	WebUI.setText(findTestObject('change_Password/Page_Cidaas Developers/input_username'), 'vyalavatti@gmail.com')
    WebUI.setText(findTestObject('change_Password/Page_Cidaas Developers/input_password'), '12345678')
	WebUI.click(findTestObject('change_Password/Page_Cidaas Developers/button_Login'))
}
catch (Exception e) {
	
//	WebUI.verifyElementPresent(findTestObject('change_Password/Page_User Profile/p_Invalid username or password'), 10)
//	WebUI.setText(findTestObject('change_Password/Page_Cidaas Developers/input_username'), 'vyalavatti@gmail.com')
//		WebUI.setText(findTestObject('change_Password/Page_Cidaas Developers/input_password'), '123456')
//		WebUI.click(findTestObject('change_Password/Page_Cidaas Developers/button_Login'))
} 

WebUI.click(findTestObject('change_Password/Page_User Profile/a_Change Password'))

WebUI.setText(findTestObject('change_Password/Page_User Profile/input_oldPassword'), '12345678')

WebUI.setText(findTestObject('change_Password/Page_User Profile/input_newPassword'), '12345678')

WebUI.setText(findTestObject('change_Password/Page_User Profile/input_reenterpassword'), '12345678')

WebUI.delay(6)

WebUI.click(findTestObject('change_Password/Page_User Profile/button_Save'))

//WebUI.click(findTestObject('change_Password/Page_User Profile/button_OK'))
WebUI.delay(6)

try {
    WebUI.verifyElementPresent(findTestObject('change_Password/Page_User Profile/div_The old password you enter'), 10)

    println('Entered Old password is wrong')

    WebUI.click(findTestObject('change_Password/Page_User Profile/button_OK'))

    WebUI.clearText(findTestObject('change_Password/Page_User Profile/input_oldPassword'))

    WebUI.setText(findTestObject('change_Password/Page_User Profile/input_oldPassword'), '12345678')

    WebUI.click(findTestObject('change_Password/Page_User Profile/button_Save'))

    WebUI.click(findTestObject('change_Password/Page_User Profile/button_OK'))

    WebUI.closeBrowser()
}
catch (Exception e) {
    println('Password changed successfully')

    WebUI.click(findTestObject('change_Password/Page_User Profile/button_OK'))

    WebUI.closeBrowser()
} 

