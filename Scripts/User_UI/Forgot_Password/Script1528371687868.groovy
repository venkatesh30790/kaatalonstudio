import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://nightlybuild.cidaas.de/user-ui/login?groupname=default&lang=en-US,en;q%3D0.9&view_type=login&redirect_uri=https:%2F%2Fnightlybuild.cidaas.de%2Fuser-profile%2Feditprofile&client_id=4d5e6e20-9347-4255-9790-5b7196843103&response_type=token&scope=openid%20profile&requestId=aced207a-9c3a-4ac8-9aa5-8df6a2442b5a')

WebUI.click(findTestObject('Forgot_Pass/Page_Cidaas Developers/a_Forgot your password'))

WebUI.setText(findTestObject('Forgot_Pass/Page_Cidaas Developers/input_email'), 'vyalavatti@gmail.com')

WebUI.click(findTestObject('Forgot_Pass/Page_Cidaas Developers/button_Reset'))

//WebUI.setText(findTestObject('Forgot_Pass/Page_Cidaas Developers/input_ver_code'), '')




try {
	WebUI.verifyElementPresent(findTestObject('Forgot_Pass/Page_Cidaas Developers/span_text-danger'), 10)
	println "User not exist"
} catch (Exception e) {
	WebUI.verifyElementPresent(findTestObject('Forgot_Pass/Page_Cidaas Developers/label_Please enter verificatio'), 10)
	println "Forgot Password working fine"
}




//WebUI.closeBrowser()

